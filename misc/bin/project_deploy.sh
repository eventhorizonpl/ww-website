#!/bin/sh

if [ $# -gt 0 ]
then
    SERVER=$1
fi

if [ "$SERVER" = "ehorizon" ]
then
    rsync -azC --force --delete --progress --exclude-from=misc/data/rsync_exclude.txt -e "ssh -p59184" ./ ehorizon@88.198.13.93:/home/ehorizon/workwatcher.pl
fi
