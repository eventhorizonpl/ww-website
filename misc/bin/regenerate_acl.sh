#!/bin/sh

sudo setfacl -R -m u:apache:rwx -m u:`whoami`:rwx var
sudo setfacl -dR -m u:apache:rwx -m u:`whoami`:rwx var
